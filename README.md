# Intro

This assignment projects helps on:

- service provision, services are: httpd / rabbitmq / postgresql
- service monitoring, monitor above installed services and check status

Tools being used in this project:

- Python, to monitor service status on server and write into json files
- Elasticsearch, to store the monitor result
- Flask, to host a web services to add json and check service status
- Ansible, to automate daily ops work
- Vagrant, simulate server hosts

# Quick start

##  1. Elasticsearch provision

We can use docker to quick provision elasticsearch here.

```bash
docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -e "ES_JAVA_OPTS=-Xms256m -Xmx256m" docker.elastic.co/elasticsearch/elasticsearch:7.9.0
```

##  2. Deploy monitor script

Put `monitor.py` under `crontab` on your Linux servers, set the interval as required(assignment didn't mention this)

NOTE: 

1.  If we are running this `monitor.py` under container environment, then `crontab` may not be an option, we may need to change the `monitor.py` script to make it as a long-live running script by adding a while-True wrapper.

2.  Actually it's better to write the monitor data into elasticsearch from `monitor.py` side, so to have monitor result immediately reflected into web service. But the assignment doesn't require this, so I didn't do it.

3.  I am using `ps` command to check service status. However, in reality, we may also need to use `systemctl`/`service` or other commands to check status. 

##  3. Run webservice
    
I use `Flask` to implement webservice here, better than Django cause more lightweight here.

```bash
cd assignment_rbc
pip install -r requirements.txt
# in production, you may need to remove the FLASK_ENV=development
FLASK_ENV=development FLASK_APP=webservice.py APP_SETTINGS=./app.cfg flask run --host=0.0.0.0
```

You can open `http://127.0.0.1:5000/` for this web service, we have 2 REST APIS:

- `POST /add`: Insert payload into Elasticsearch

- `GET /health_check`: Return the Application status ("UP" or "DOWN")

##  4. Ingest some data

```bash
cd assignment_rbc
# gen data, optional, since I already have some data samples in data directory
python monitor.py
cd data
# this is used to insert all created json files.
# As mentioned above, in general, data insert should happen during monitor.py script 
bash batch_insert.sh
# this is used to bulk delete if you want to delete all data, just for debug
bash batch_delete.sh
```

## 5. Check Service status by running ansible playbook

Inventory file location is defined in `ansible/ansible.cfg`
```bash
cd ansible
ansible-playbook main.yml -e 'action=check-status' 
```

## 6. Daily ops work using Ansible

Describe in next section.

# Ansible DevOps work
    
##  1. Ensure you have three hosts up and running
If you don't have them I have used [vagrant](https://www.vagrantup.com/) to simulate three Ubuntu hosts.

```bash
cd ansible/servers
# start the servers
bash start_servers.sh
# stop the servers
bash stop_servers.sh
```

##  2. Test hosts connection

```bash
cd ansible
ansible -i inventory.hosts machines -m ping 
```

##  3. Verify and Install services

```bash
cd ansible
ansible-playbook main.yml -e 'action=verify_install'
```

NOTE:

1.  `action` is a reserved name in `ansible`, better to use other var name.

2.  `verify_install` naming style is different from other twos, better to unify them.

##  4. Check disk and send email

```bash
cd ansible
ansible-playbook main.yml -e 'action=check-disk'
```

Here is a sample email alert when you have `critical/warn` level disk partition status:

![pic](ansible/email_alert.png)

NOTE:

1.  Please change `alert_email` and `disk_alert_threshold` values in `main.yml` if necessary

2.  Please configure your own email/password in `check-disk.yml`

3.  Better to use `vault` to encrypt password mentioned above

##  5. Check service status via webservice
    
Already demonstrate in above monitor section.

```bash
cd ansible
ansible-playbook main.yml -e 'action=check-status' 
```