# coding: utf-8

"""
This script provide with a REST webservice that:
Accepts the created JSON files and writes it to Elasticsearch
Provide a second endpoint where the data can be retrieved, i.e

POST /add -> Insert payload into Elasticsearch
GET /health_check -> Return the Application status (“UP” or “DOWN”)

"""

import flask
from flask import Flask
from flask import request
from flask import jsonify
from elasticsearch import Elasticsearch

import re

app = Flask(__name__)
app.config.from_envvar('APP_SETTINGS')

LOG = app.logger

# usage: https://github.com/elastic/elasticsearch-py#example-use
es = Elasticsearch(hosts=[
    {"host": app.config['ES_HOST'], "port": app.config['ES_PORT']}
])
es_index_name = app.config['ES_INDEX_NAME']

"""
result, means , response is okay or not from server, not indicating services status
this is for exception handling
"""
PAYLOAD_OK = {"result": "ok"}
PAYLOAD_ERROR = {"result": "error"}


@app.route('/add', methods=['POST'])
def add_svc_status():
    file = request.files['file']
    LOG.debug('uploaded filename: %s', file.filename)
    # should be {serviceName}-status-{@timestamp}.json pattern
    m = re.search('(.+)\-status\-@(\d{10,}).json', file.filename)
    if not m:
        raise AppError(
            'File name pattern should be {serviceName}-status-{@timestamp}.json. eg, httpd-status-@1598652934.json',
            status_code=410, payload=PAYLOAD_ERROR)
    service_name_from_fname = m.group(1)
    timestamp = m.group(2)
    monitor_obj = flask.json.load(file)
    LOG.debug('uploaded monitor obj: %s', monitor_obj)
    if monitor_obj['service_name'] != service_name_from_fname:
        raise AppError(
            'Service name from the file name is not matched with the json file content.',
            status_code=410, payload=PAYLOAD_ERROR)
    monitor_obj['timestamp'] = int(timestamp)
    id = timestamp + '-' + service_name_from_fname
    result = es.index(index=es_index_name, body=monitor_obj, id=id)
    return PAYLOAD_OK


def query_svc_status(svc_name="httpd"):
    query_res = es.search(index=es_index_name,
                        body={
                            "query": {"match": {"service_name": svc_name}},
                            "sort": [{"timestamp": {"order": "desc"}}],
                            "size": 1
                        })
    if query_res['hits']['total']['value'] == 0:
        raise AppError('No monitor data found.',
                       status_code=500, payload=PAYLOAD_ERROR)
    return query_res['hits']['hits'][0]['_source']["service_status"]

@app.route('/health_check', methods=['GET'])
def health_check():
    svc_status = {}
    app_status = "UP"
    svc_list = ["httpd", "rabbitmq", "postgresql"]
    for svc in svc_list:
        # TODO: optimize, to combine 3 query requests to one request
        status = query_svc_status(svc)
        svc_status[svc] = status
        if status == "DOWN":
            app_status = "DOWN"
    payload = PAYLOAD_OK
    payload['status'] = app_status
    payload['services'] = svc_status
    return payload


"""
For exception handling
"""


class AppError(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@app.errorhandler(AppError)
def handle_app_error(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
