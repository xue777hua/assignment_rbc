# coding: utf-8

"""
This python script will monitors three services (httpd/RabbitMQ/postgreSQL) and creates a JSON
  object with application_name, application_status and host_name. Next is a sample JSON Payload:

{
   "service_name":"httpd",
   "service_status":"UP",
   "host_name":"host1"
}
Please write this JSON object to a file named {serviceName}-status-{@timestamp}.json

"""

import os
import time
import json

MONITOR_DATA_DIR = "./data"

SVC_CHECK_CMD_TEMPLATE1 = "systemctl status > /dev/null 2>&1 %s > /dev/null"
SVC_CHECK_CMD_TEMPLATE2 = "ps aux|grep \"%s\"|grep -v grep > /dev/null 2>&1"

SVC_CHECK = {
    "rabbitmq": SVC_CHECK_CMD_TEMPLATE2 % ("rabbitmq"),
    "httpd": SVC_CHECK_CMD_TEMPLATE2 % ("httpd"),
    "postgresql": SVC_CHECK_CMD_TEMPLATE2 % ("postgresql")
}


def write_monitor_result(service_name, service_status):
    template = """
{
  "service_name": "%s",
  "service_status": "%s",
  "host_name": "host1"
}
    """ % (service_name, service_status)


def check_service_status():
    for k, v in SVC_CHECK.items():
        svc_monitor_obj = {
            "service_name": k,
            "service_status": "UP",
            "host_name": "host1"
        }
        status = os.system(v)
        if status != 0:
            svc_monitor_obj["service_status"] = "DOWN"
        # write monitor result to a json file, named as {serviceName}-status-{@timestamp}.json
        timestamp = int(time.time())
        json_file_name = "%s/%s-status-@%s.json" % (MONITOR_DATA_DIR, k, timestamp)
        with open(json_file_name, 'w') as outfile:
            json.dump(svc_monitor_obj, outfile)


check_service_status()
