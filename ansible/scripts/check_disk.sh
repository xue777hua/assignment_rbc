#!/usr/bin/env bash

print_usage() {
  echo "[ERROR] error parsing alert levels arguments."
  echo "        Usage: $0 [warn_level_pct] [critical_level_pct]. eg. $0 80 90"
  echo "        Output explanation: "
  echo "               - contains 'status: ok': all disk partitions are in good state"
  echo "               - contains 'status: warn': at least 1 disk partition is in warn level"
  echo "               - contains 'status: critical': at least 1 disk partition is in critical level"
  exit 1
}

[ $# -eq 2 ] || print_usage

warn_level_pct=${1-80}
critical_level_pct=${2-90}

# get disk usage
disk_usage_output=`df -H | grep -vE '^Filesystem|tmpfs|cdrom|vagrant' | awk '{ print $5 " " $1 " " $6}' | tr '%' ' '`
IFS=$'\n'
warn_list=""
critical_list=""
for l in $disk_usage_output; do
  ratio=`echo $l | cut -d ' ' -f1`
  [ ${ratio} -lt ${warn_level_pct} ] || warn_list+=" | [WARN] ${l}"
  [ ${ratio} -lt ${critical_level_pct} ] || critical_list+=" | [CRITICAL] ${l}"
done


# compute status and print report
status="ok"
[ "${warn_list}" == "" ] || status="warn"
[ "${critical_list}" == "" ] || status="critical"

report="status: ${status}"
[ "${warn_list}" == "" ] || report+=${warn_list}
[ "${critical_list}" == "" ] || report+=${critical_list}

echo ${report}